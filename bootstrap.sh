#!/bin/bash

LOGFILE="/tmp/bootstrap.log"

cd /tmp
echo "" > $LOGFILE

echo -ne "\n\nPuppet Bootstrap\n"
echo "---------------------------------------"
echo -ne "\n"

if [ "$1" == "--help" ] || [ "$1" == "-h" ] || [ "$1" == "?" ]; then
echo -ne "Usage: $0 [ role_id1 role_id2... ]\n\n"
exit 100
fi

CONFIGDIR=/tmp/puppet

echo -ne "Detecting OS... "
if [ ! -f "/usr/bin/yum" ]; then
echo -ne "Unsupported OS. Exiting.\n"
exit 1
fi
echo -ne "Red Hat based\n"

echo -ne "Detecting Git... "
if [ ! -f "/usr/bin/git" ]; then
/usr/bin/yum install git -y &> $LOGFILE
fi

if [ ! -f "/usr/bin/git" ]; then
echo -ne "Failed. See $LOGFILE for details.\n"
exit 1
else
echo -ne "Installed\n"
fi

read -e -p "Enter your BitBucket username: " username
echo -ne "Enter your BitBucket password: "
read -e -s password
echo ""

if [ -z "${username}" ]; then
echo "Error: No BitBucket credentials specified."
exit
fi


GIT_REPO="https://${username}:${password}@bitbucket.org/flashtalkingprocessing/processing-node-puppet-m247.git"

echo -ne "Cloning Puppet configs... "
/bin/rm -rf $CONFIGDIR
if /usr/bin/git clone $GIT_REPO $CONFIGDIR &> $LOGFILE; then
    echo -ne "Done\n"
else
    echo -ne "Failed. Exiting. See $LOGFILE for details.\n"
    exit 2
fi

echo -ne "Detecting Puppet... "

if [ ! -f "/usr/bin/puppet" ]; then
/usr/bin/yum install -y https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-7.noarch.rpm &> $LOGFILE
/usr/bin/yum install puppet -y &> $LOGFILE
fi

if [ ! -f "/usr/bin/puppet" ]; then
echo -ne "Failed. Are you root?\n"
exit 1
else
echo -ne "Installed\n"
fi

echo -ne "Applying default Puppet configs... "
if [ -f "$CONFIGDIR/manifests/init.pp" ]; then
    if /usr/bin/puppet apply --modulepath=$CONFIGDIR/modules $CONFIGDIR/manifests/init.pp &> $LOGFILE; then
        echo -ne "Done\n"
    else
        echo -ne "Failed. Exiting. See $LOGFILE for details.\n"
        exit 3
    fi
else
    echo -ne "Failed. Could not clone Git repo. Exiting. See $LOGFILE for details.\n"
    exit 4
fi

if [ -z $1 ]; then
    echo -ne "No role specific role was provided.\n"
else

# Validate the provided roles first
for ROLE_ID in "$@"
do
if [ "$ROLE_ID" == "init" ]; then
echo -ne "Cannot specify "init" as a role id\n\n"
exit 100
fi

if [[ ! -f "$CONFIGDIR/manifests/$ROLE_ID.pp" ]]; then
echo -ne "Failed to install role specific config $ROLE_ID. Manifest could not be found.\n"
fi
done

for ROLE_ID in "$@"
do
echo -ne "Applying config for role: $ROLE_ID... "
if /usr/bin/puppet apply --modulepath=$CONFIGDIR/modules  $CONFIGDIR/manifests/$ROLE_ID.pp &> $LOGFILE; then
echo -ne "Done\n"
else
echo -ne "\nFailed to install role specific config $ROLE_ID. See $LOGFILE for details.\n"
exit 5
fi
done
fi


echo -ne "Bootstrap complete.\nLog output:\n\n"

cat $LOGFILE

echo -ne "\n"

echo -ne "Don't forget to remove the log file: rm $LOGFILE\n\n"